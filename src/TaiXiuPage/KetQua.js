import React from "react";

export default function KetQua({
  luaChon,
  handlePlayGame,
  soBanThang,
  soLuotChoi,
  ketQua,
}) {
  return (
    <div>
      <button onClick={handlePlayGame} className="btn btn-warning px-5 py-3">
        Play Game
      </button>
      {luaChon && (
        <h2 className="display-4 text-danger">Bạn Chọn: {luaChon}</h2>
      )}
      {luaChon && <h2 className="text-danger">Kết Quả: {ketQua}</h2>}
      <h2 className="py-3">Số bàn Thắng: {soBanThang}</h2>
      <h2>Số Lượt Chơi: {soLuotChoi}</h2>
    </div>
  );
}
