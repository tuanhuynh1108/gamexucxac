import React from "react";
import { TAI, XIU } from "./TaiXiuPage";

export default function Taixiu({ xucXacArr, handleDatCuoc }) {
  const renderXucXacArr = () => {
    return xucXacArr.map((item, index) => {
      return (
        <img style={{ height: 100, margin: 10 }} src={item.img} key={index} />
      );
    });
  };
  return (
    <div className="d-flex justify-content-between container  p-5">
      <button
        onClick={() => {
          handleDatCuoc(XIU);
        }}
        className="btn btn-secondary p-5"
      >
        Xỉu
      </button>
      <div>{renderXucXacArr()}</div>

      <button
        onClick={() => {
          handleDatCuoc(TAI);
        }}
        className="btn btn-danger p-5"
      >
        Tài
      </button>
    </div>
  );
}
