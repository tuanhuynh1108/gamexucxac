import React, { useState } from "react";
import bgGame from "../assets/bgGame.png";
import "./bgGame.css";
import KetQua from "./KetQua";
import Taixiu from "./Taixiu";
export const TAI = "Tài";
export const XIU = "Xỉu";
export default function TaiXiuPage() {
  const [xucXacArr, setXucXacArr] = useState([
    {
      img: "./imgXucXac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucXac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucXac/1.png",
      giaTri: 1,
    },
  ]);
  const [luaChon, setLuaChon] = useState(null);
  const [soBanThang, setSoBanThang] = useState(0);
  const [soLuotChoi, setSoLuotChoi] = useState(0);
  const [ketQua, setKetQua] = useState(null);
  const handleDatCuoc = (value) => {
    setLuaChon(value);
  };
  let tongDiem = 0;

  const handlePlayGame = () => {
    // Math.floor(Math.random() * (max - min + 1) + min)
    let newXucXacArr = xucXacArr.map(() => {
      // setInterval(() => {
      let number = Math.floor(Math.random() * (6 - 1 + 1) + 1);
      tongDiem += number;
      return {
        img: `./imgXucXac/${number}.png`,
        giaTri: number,
      };
    });

    setXucXacArr(newXucXacArr);
    setSoLuotChoi(soLuotChoi + 1);
    tongDiem >= 11 && luaChon == TAI && setSoBanThang(soBanThang + 1);
    tongDiem < 11 && luaChon == XIU && setSoBanThang(soBanThang + 1);

    tongDiem >= 11 && luaChon == TAI && setKetQua("YOU WIN");
    tongDiem >= 11 && luaChon == XIU && setKetQua("YOU LOSE");

    tongDiem < 11 && luaChon == XIU && setKetQua("YOU WIN");
    tongDiem < 11 && luaChon == TAI && setKetQua("YOU LOSE ");
  };

  return (
    <div
      style={{ backgroundImage: `url(${bgGame})` }}
      className="bgGame text-center text-danger  "
    >
      <p className="display-4 text-secondary">Game Xúc Xắc</p>
      <Taixiu handleDatCuoc={handleDatCuoc} xucXacArr={xucXacArr} />
      <KetQua
        ketQua={ketQua}
        soBanThang={soBanThang}
        soLuotChoi={soLuotChoi}
        handlePlayGame={handlePlayGame}
        luaChon={luaChon}
      />
    </div>
  );
}
