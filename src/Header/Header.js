import React from "react";
import { NavLink } from "react-router-dom";
import "./headerGame.css";
import bg_header from "../assets/bg_header.png";

export default function Header() {
  return (
    <div
      className="header_game text-center "
      style={{ backgroundImage: `url(${bg_header})` }}
    >
      <NavLink to="/game-tai-xiu" className=" display-1  text-success  ">
        Chơi Ngay
      </NavLink>
    </div>
  );
}
